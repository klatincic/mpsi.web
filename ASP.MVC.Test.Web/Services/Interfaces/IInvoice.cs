﻿using ASP.MVC.Test.Web.Models;
using ASP.MVC.Test.Web.Services.Requests;
using System.Threading.Tasks;
using ASP.MVC.Test.Web.Models.ResponseModels;

namespace ASP.MVC.Test.Web.Services.Interfaces
{
    interface IInvoice
    {
        Task<NewInvoiceModel> NewInvoice(PostInvoiceRequest postInvoiceRequest);
        Task<InvoiceModel> ReturnMyInvoices(GetReturnMyInvoicesRequest getReturnMyInvoicesRequest, int x);
        Task<InvoiceById> SearchInvoiceById(GetSearchInvoiceById getSearchInvoiceById, int x);
        Task<CostCenters> ReturnCostCenters(GetCostCenters getCostCenters);
    }
}
