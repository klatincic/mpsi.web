﻿using System.Threading.Tasks;
using ASP.MVC.Test.Web.Models;
using ASP.MVC.Test.Web.Models.ResponseModels;
using ASP.MVC.Test.Web.Services.Requests;

namespace ASP.MVC.Test.Web.Services.Interfaces
{
    interface IUserInfo
    {
        Task<UserModel> UserInfo(GetUserRequest getUserRequest);
    }
}