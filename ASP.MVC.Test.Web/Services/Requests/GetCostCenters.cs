﻿using ASP.MVC.Test.Web.Services.Requests.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.MVC.Test.Web.Services.Requests
{
    public class GetCostCenters:BaseGetRequest
    {
        public string QueryString { get; } = "/api/invoice/costcenter";
    }
}