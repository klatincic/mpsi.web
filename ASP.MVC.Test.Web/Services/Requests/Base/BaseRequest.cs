﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace ASP.MVC.Test.Web.Services.Requests.Base
{
    public class BaseRequest
    {
        public string ApiEndPoint { get; } = "http://mpsi.celebesit.hr/mobile";
        public HttpClient HttpClient { get; set; }
        public BaseRequest()
        {
            var cookieContainer = new CookieContainer();
            var handler = GetHttpClientHandler(cookieContainer);

            var httpClient = new HttpClient(handler)
            {
                BaseAddress = new Uri(ApiEndPoint)
            };

            HttpClient = httpClient;

            //httpClient.DefaultRequestHeaders.Accept.Add(
            //    new MediaTypeWithQualityHeaderValue("application/json"));

            //if (GlobalSettings.Instance.JSessionCookie != null)
            //{
            //    cookieContainer.Add(httpClient.BaseAddress,
            //        new Cookie(GlobalSettings.Instance.JSessionCookie.Name,
            //            GlobalSettings.Instance.JSessionCookie.Value));
            //}
        }

        private HttpClientHandler GetHttpClientHandler(CookieContainer cookieContainer)
        {
            return new HttpClientHandler()
            {
                CookieContainer = cookieContainer
            };
        }
    }
}