﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.MVC.Test.Web.Services.Requests.Base
{
    public class BaseGetRequest : BaseRequest
    {
        public string AuthorizationKey { get; } = "Authorization";

        public string AuthorizationValue { get; } = "Bearer " + HttpContext.Current.Session["AccessToken"];
    }
}