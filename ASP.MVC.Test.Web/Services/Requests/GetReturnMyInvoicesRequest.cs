﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASP.MVC.Test.Web.Services.Requests.Base;

namespace ASP.MVC.Test.Web.Services.Requests
{
    public class GetReturnMyInvoicesRequest:BaseGetRequest
    {

        public string QueryString { get; } = "/api/invoice/Search/my?pageNumber=";
    }
}