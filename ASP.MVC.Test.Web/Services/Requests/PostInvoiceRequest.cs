﻿using System.Web;
using ASP.MVC.Test.Web.Services.Requests.Base;
using ASP.MVC.Test.Web.Models;

namespace ASP.MVC.Test.Web.Services.Requests
{
    public class PostInvoiceRequest : BaseRequest
    {
        public NewInvoiceModel NewInvoiceModel { get; set; }

        public string QueryString { get; } = "/api/invoice";

        public string ContentTypeKey { get; } = "Content-Type";

        
        public string ContentTypeValue { get; } = "application/json";
        
        
        public string InvoiceTypeKey { get;  } = "InvoiceType";

        public string InvoiceTypeValue { get; set; } 

        public string PaymentTypeKey { get;  } = "PaymentType";

        public string PaymentTypeValue { get; set; }

        //public string InvoiceImageKey { get; } = "InvoiceImage";

        //public string InvoiceImageValue { get; set; }

        public string AuthorizationKey { get; } = "Authorization";

        public string AuthorizationValue { get; } = "Bearer " + HttpContext.Current.Session["AccessToken"];
    }
}