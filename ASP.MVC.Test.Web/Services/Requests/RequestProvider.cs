﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System.Text;

namespace ASP.MVC.Test.Web.Services.Requests
{
    public class RequestProvider
    {
        private JsonSerializerSettings _serializerSettings;
        private HttpClient _httpClient;
        public RequestProvider(HttpClient httpClient)
        {
            _httpClient = httpClient;
            _serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                NullValueHandling = NullValueHandling.Ignore,
                Converters = new List<JsonConverter> { new StringEnumConverter { CamelCaseText = true } },

            };
            _serializerSettings.Converters.Add(new StringEnumConverter());
        }
        public async Task<TResult> GetAsync<TResult>(string path)
        {
            using (var httpClient = _httpClient)
            {

                var response = await httpClient.GetAsync(path);

                //await HandleResponse(response);
                var serialized = await response.Content.ReadAsStringAsync();

                var result = await Task.Run(() =>
                    JsonConvert.DeserializeObject<TResult>(serialized, _serializerSettings));

                Handleresponse(response, result);

                return result;
            }
        }

        public async Task<TResult> GetAsync<TResult>(string path, List<KeyValuePair<string, string>> headerParameters = null)
        {
            using (var httpClient = _httpClient)
            {
                httpClient.DefaultRequestHeaders.Add("Authorization", headerParameters.FirstOrDefault(x => x.Key == "Authorization").Value);

                var response = await httpClient.GetAsync(path);

                //await HandleResponse(response);
                var serialized = await response.Content.ReadAsStringAsync();

                var result = await Task.Run(() =>
                    JsonConvert.DeserializeObject<TResult>(serialized, _serializerSettings));

                Handleresponse(response, result);

                return result;
            }
        }

        public async Task<TResult> PostAsync<TResult>(string uri, string data)
        {
            using (var httpClient = _httpClient)
            {
                var content = new StringContent(data);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/text");

                var response = await httpClient.PostAsync(uri, content);

                //await HandleResponse(response);
                var serialized = await response.Content.ReadAsStringAsync();

                var result = await Task.Run(() => JsonConvert.DeserializeObject<TResult>(serialized, _serializerSettings));

                Handleresponse(response, result);

                return result;
            }
        }

        public async Task<TResult> PostAsync<TResult>(string uri, List<KeyValuePair<string, string>> formParameters, List<KeyValuePair<string, string>> headerParameters)
        {
            using (var httpClient = _httpClient)
            {
                FormUrlEncodedContent content = new FormUrlEncodedContent(formParameters);
                content.Headers.ContentType =
                    new MediaTypeHeaderValue(headerParameters.FirstOrDefault(x => x.Key == "Content-Type").Value);

                foreach (KeyValuePair<string, string> kvp in headerParameters)
                {
                    if (kvp.Key != "Content-Type")
                        content.Headers.Add(kvp.Key, kvp.Value);
                }

                var response = await httpClient.PostAsync(uri, content);

                //await HandleResponse(response);
                var serialized = await response.Content.ReadAsStringAsync();

                var result = await Task.Run(() =>
                    JsonConvert.DeserializeObject<TResult>(serialized, _serializerSettings));

                Handleresponse(response, result);

                return result;
            }
        }

        public async Task<TResult> PostAsync<TResult>(string uri, string bodyJsonParameters, List<KeyValuePair<string, string>> headerParameters)
        {
            using (var httpClient = _httpClient)
            {

                try
                {
                    var content = new StringContent(bodyJsonParameters, Encoding.UTF8, "application/json");

                    content.Headers.ContentType =
                        new MediaTypeHeaderValue(headerParameters.FirstOrDefault(x => x.Key == "Content-Type").Value);

                    httpClient.DefaultRequestHeaders.Add("Authorization", headerParameters.FirstOrDefault(x => x.Key == "Authorization").Value);

                    var response = await httpClient.PostAsync(uri, content);

                    //await HandleResponse(response);
                    var serialized = await response.Content.ReadAsStringAsync();

                    var result = await Task.Run(() =>
                        JsonConvert.DeserializeObject<TResult>(serialized, _serializerSettings));

                    Handleresponse(response, result);

                    return result;
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        public async Task<TResult> PutAsync<TResult>(string uri, string data)
        {
            using (var httpClient = _httpClient)
            {
                //if (!string.IsNullOrEmpty(header))
                //{
                //    AddHeaderParameter(httpClient, header);
                //}

                //var content = new StringContent(JsonConvert.SerializeObject(data));
                var content = new StringContent(data);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                var response = await httpClient.PutAsync(uri, content);

                //await HandleResponse(response);
                var serialized = await response.Content.ReadAsStringAsync();

                var result = await Task.Run(() => JsonConvert.DeserializeObject<TResult>(serialized, _serializerSettings));

                Handleresponse(response, result);

                return result;
            }
        }

        

        private void Handleresponse<TResult>(HttpResponseMessage response, TResult result)
        {
            string message = string.Empty;
            if (!response.IsSuccessStatusCode)
                message = response.ReasonPhrase;
            if (result == null)
                throw new ApplicationException(message);
        }
    }
}