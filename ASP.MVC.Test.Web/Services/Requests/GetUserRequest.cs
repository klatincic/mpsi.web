﻿using ASP.MVC.Test.Web.Services.Requests.Base;

namespace ASP.MVC.Test.Web.Services.Requests
{
    public class GetUserRequest: BaseGetRequest
    {
        public string QueryString { get; } = "/api/User/me";
    }
}