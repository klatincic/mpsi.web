﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASP.MVC.Test.Web.Services.Interfaces;
using ASP.MVC.Test.Web.Models;
using ASP.MVC.Test.Web.Services.Requests;
using System.Threading.Tasks;
using ASP.MVC.Test.Web.Models.ResponseModels;
using Newtonsoft.Json;

namespace ASP.MVC.Test.Web.Services
{
    public class InvoiceService:IInvoice
       
    {

        public async Task<CostCenters> ReturnCostCenters(GetCostCenters request)
        {
            var headerParameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>(request.AuthorizationKey, request.AuthorizationValue)
            };

            var requestProvider = new RequestProvider(request.HttpClient);
            var path = $"{request.ApiEndPoint}{request.QueryString}";
            return await requestProvider.GetAsync<CostCenters>(path, headerParameters);

        }
        public async Task<NewInvoiceModel> NewInvoice(PostInvoiceRequest request)
        {
            var path = request.ApiEndPoint + request.QueryString;
           
            List<KeyValuePair<string, string>> headerParameters = new List<KeyValuePair<string, string>>();
            headerParameters.Add(new KeyValuePair<string, string>(request.ContentTypeKey, request.ContentTypeValue));
            headerParameters.Add(new KeyValuePair<string, string>(request.AuthorizationKey, request.AuthorizationValue));

            RequestProvider requestProvider = new RequestProvider(request.HttpClient);

            string bodyJsonParametes = JsonConvert.SerializeObject(request.NewInvoiceModel);

            return await requestProvider.PostAsync<NewInvoiceModel>(path, bodyJsonParametes, headerParameters);
        }

        public async Task<InvoiceModel> ReturnMyInvoices(GetReturnMyInvoicesRequest request, int pageNumber)
        {
            var headerParameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>(request.AuthorizationKey, request.AuthorizationValue)
            };

            var requestProvider = new RequestProvider(request.HttpClient);
            var path = $"{request.ApiEndPoint}{request.QueryString}{pageNumber}";
            return await requestProvider.GetAsync<InvoiceModel>(path, headerParameters);
        }

        public async Task<InvoiceById> SearchInvoiceById(GetSearchInvoiceById request, int id)
        {
            var headerParameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>(request.AuthorizationKey, request.AuthorizationValue)
            };

            var requestProvider = new RequestProvider(request.HttpClient);
            var path = $"{request.ApiEndPoint}{request.QueryString}{id}";
            return await requestProvider.GetAsync<InvoiceById>(path, headerParameters);
        }

    }
}