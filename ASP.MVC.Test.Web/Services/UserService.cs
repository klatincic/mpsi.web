﻿using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using ASP.MVC.Test.Web.Models;
using ASP.MVC.Test.Web.Models.ResponseModels;
using ASP.MVC.Test.Web.Services.Interfaces;
using ASP.MVC.Test.Web.Services.Requests;

namespace ASP.MVC.Test.Web.Services
{
    public class UserService:IUserInfo
    {
        public async Task<UserModel> UserInfo(GetUserRequest request)
        {
            List<KeyValuePair<string, string>> headerParameters = new List<KeyValuePair<string, string>>();
            headerParameters.Add(new KeyValuePair<string, string>(request.AuthorizationKey, request.AuthorizationValue));

            RequestProvider requestProvider = new RequestProvider(request.HttpClient);
        
            var path = request.ApiEndPoint + request.QueryString;
            return await requestProvider.GetAsync<UserModel>(path,headerParameters);

        }
    }
}