﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ASP.MVC.Test.Web.Models;
using ASP.MVC.Test.Web.Models.ResponseModels;
using ASP.MVC.Test.Web.Services.Interfaces;
using ASP.MVC.Test.Web.Services.Requests;

namespace ASP.MVC.Test.Web.Services
{
    public class AuthenticationService : IAuthentication
    {
        public async Task<ConnectModel> Login(PostAuthenticationRequest request)
        {
            var path = request.ApiEndPoint + request.QueryString;
            List<KeyValuePair<string, string>> formParameters = new List<KeyValuePair<string, string>>();
            formParameters.Add(new KeyValuePair<string, string>(request.GrantTypeKey, request.GrantTypeValue));
            formParameters.Add(new KeyValuePair<string, string>(request.UserNameKey, request.UserNameValue));
            formParameters.Add(new KeyValuePair<string, string>(request.PasswordKey, request.PasswordValue));
            formParameters.Add(new KeyValuePair<string, string>(request.ClientIdKey, request.ClientIdValue));
            formParameters.Add(new KeyValuePair<string, string>(request.DeviceSerialNumberKey, request.DeviceSerialNumberValue));

            List<KeyValuePair<string, string>> headerParameters = new List<KeyValuePair<string, string>>();
            headerParameters.Add(new KeyValuePair<string, string>(request.ContentTypeKey, request.ContentTypeValue));

            RequestProvider requestProvider = new RequestProvider(request.HttpClient);

            return await requestProvider.PostAsync<ConnectModel>(path, formParameters, headerParameters);
        }
    }
}