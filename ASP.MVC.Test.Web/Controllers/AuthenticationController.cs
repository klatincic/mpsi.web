﻿using System.Threading.Tasks;
using System.Web.Mvc;
using ASP.MVC.Test.Web.Models;
using ASP.MVC.Test.Web.Models.ResponseModels;
using ASP.MVC.Test.Web.Services;
using ASP.MVC.Test.Web.Services.Requests;
using System.Web.Security;

namespace ASP.MVC.Test.Web.Controllers
{
    public class AuthenticationController : Controller
    {
        // GET: Authentication
        public ActionResult Index()
        {
            LoginModel lModel = new LoginModel();
            return View(lModel);

        }
        
        [HttpPost]
        public async Task<ActionResult> Login(LoginModel loginModel)
        {
            
            if (ModelState.IsValid)
            {
                PostAuthenticationRequest request = new PostAuthenticationRequest() { UserNameValue = loginModel.UserName, PasswordValue = loginModel.Password };
                AuthenticationService authenticationService = new AuthenticationService();
                
                var retVal = await Task.Run(async () => await authenticationService.Login(request));
                
                if (!string.IsNullOrEmpty(retVal.Error))
                {
                    LoginModel lModel = new LoginModel
                    {
                        Error = retVal.Error,
                        ErrorDescription = retVal.ErrorDescription
                    };
                    
                    return View("Index", lModel);
                }
                else
                {
                    retVal.Error = " ";

                    Session["AccessToken"] = retVal.AccessToken;
                    Session["ExpiresIn"] = retVal.ExpiresIn.ToString();
                    Session["RefreshToken"] = retVal.RefreshToken;
                    Session["Scope"] = retVal.Scope;
                    Session["TokenType"] = retVal.TokenType;
                    
                    //za ime i prezime usera u headeru
                    GetUserRequest getUserRequest = new GetUserRequest();
                    UserService userService = new UserService();

                    var retValUser = await Task.Run(async () => await userService.UserInfo(getUserRequest));

                    Session["ime"] = retValUser.FirstName;
                    Session["prezime"] = retValUser.LastName;

                    return RedirectToAction("Index", "Invoice"); 
                }
            }
            return View("Index");
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Index", "Authentication");
        }
    }
}