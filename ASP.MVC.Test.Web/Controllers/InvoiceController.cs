﻿using System.Threading.Tasks;
using System.Web.Mvc;
using ASP.MVC.Test.Web.Models;
using ASP.MVC.Test.Web.Services;
using ASP.MVC.Test.Web.Services.Requests;
using ASP.MVC.Test.Web.Models.ResponseModels;

using System.Web;
using System.IO;
using System.Collections.Generic;
using System;

namespace ASP.MVC.Test.Web.Controllers
{
    public class InvoiceController : Controller
    {

        //public ActionResult FileUpload(HttpPostedFileBase file)
        //{
        //    if (file != null)
        //    {
        //        string pic = System.IO.Path.GetFileName(file.FileName);
        //        string path = System.IO.Path.Combine(
        //                               Server.MapPath("~/images/profile"), pic);
        //        // file is uploaded
        //        file.SaveAs(path);

        //        // save the image path path to the database or you can send image 
        //        // directly to database
        //        // in-case if you want to store byte[] ie. for DB
        //        using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
        //        {
        //            file.InputStream.CopyTo(ms);
        //            byte[] array = ms.GetBuffer();
        //        }

        //    }
        //    // after successfully uploading redirect the user
        //    return RedirectToAction("NewInvoice", "Invoice");
        //}
        public async Task<ActionResult> Index()
        {
            InvoiceModel model = new InvoiceModel();
            GetReturnMyInvoicesRequest request = new GetReturnMyInvoicesRequest();
            InvoiceService invoiceService = new InvoiceService();

            var returnValue = await Task.Run(async () => await invoiceService.ReturnMyInvoices(request, 1));

            GetCostCenters requestCostCenters = new GetCostCenters();
            var retValCostCenters = await Task.Run(async () => await invoiceService.ReturnCostCenters(requestCostCenters));
           
            List<SelectListItem> list = new List<SelectListItem>();
            for(int i = 0; i < retValCostCenters.data.Count; i++)
            { 
                 list.Add(new SelectListItem { Text = retValCostCenters.data[i].CostCenterName, Value = retValCostCenters.data[i].CostCenterId.ToString() } );
            }
           
            ViewBag.Centri = list;
            
            return View(returnValue);
        }

        [HttpPost]
        public async Task<ActionResult> _NewInvoice(NewInvoiceModel newInvoiceModel)
        {
            if (ModelState.IsValid)
            {
                PostInvoiceRequest request = new PostInvoiceRequest() { NewInvoiceModel = newInvoiceModel, InvoiceTypeValue = newInvoiceModel.InvoiceType, PaymentTypeValue = newInvoiceModel.PaymentType };
                InvoiceService invoiceService = new InvoiceService();
                var retVal = await Task.Run(async () => await invoiceService.NewInvoice(request));
            }

            return RedirectToAction("Submission", "Invoice");
        }

        public ActionResult _MyInvoices()
        {
            InvoiceModel lModel = new InvoiceModel();
            return View(lModel);
        }

        public async Task<ActionResult> GetInvoiceData(int page)
        {
            var invoiceService = new InvoiceService();
            var tempModel = await invoiceService.ReturnMyInvoices(new GetReturnMyInvoicesRequest(), page);

            return View(tempModel.Podaci);
        }

        public async Task<ActionResult> FilterInvoiceData(string query)
        {
            var invoiceService = new InvoiceService();

            var podaci = new List<Podaci>();
            InvoiceModel tempInvoiceModel = null;
            
            for (int page = 1; page == 1 || tempInvoiceModel.Podaci.Count == 0; page++)
            {
                tempInvoiceModel = await invoiceService.ReturnMyInvoices(new GetReturnMyInvoicesRequest(), page);
                podaci.AddRange(tempInvoiceModel.Podaci);
            }

            podaci = podaci.FindAll(podatak => podatak.InvoiceId.ToString().Contains(query));
            tempInvoiceModel.Podaci = podaci;
            
            return View(tempInvoiceModel);
        }

        public async Task<ActionResult> FilterInvoiceDataByDate(string queryDate)
        {
            var invoiceService = new InvoiceService();

            var podaci = new List<Podaci>();
            InvoiceModel tempInvoiceModel = null;

            for (int page = 1; page == 1 || tempInvoiceModel.Podaci.Count == 0; page++)
            {
                tempInvoiceModel = await invoiceService.ReturnMyInvoices(new GetReturnMyInvoicesRequest(), page);
                podaci.AddRange(tempInvoiceModel.Podaci);
            }

            podaci = podaci.FindAll(podatak => podatak.CreatedDate.Date.ToShortDateString().Contains(queryDate));
            tempInvoiceModel.Podaci = podaci;

            return View(tempInvoiceModel);
        }

        public async Task<ActionResult> InvoiceDetails(int id)
        {

            GetSearchInvoiceById request = new GetSearchInvoiceById();
            InvoiceService invoiceService = new InvoiceService();

            var returnValue = await Task.Run(async () => await invoiceService.SearchInvoiceById(request, id));
            return View(returnValue);
        }

        public async Task<ActionResult> Submission()
        {
            InvoiceModel model = new InvoiceModel();
            GetReturnMyInvoicesRequest request = new GetReturnMyInvoicesRequest();
            InvoiceService invoiceService = new InvoiceService();
            var returnValue = await Task.Run(async () => await invoiceService.ReturnMyInvoices(request, 1));
            return View(returnValue);
            
        }
       
        //[HttpPost]
        //public ActionResult Upload(HttpPostedFileBase file)
        //{
        //    if (file != null && file.ContentLength > 0)
        //    {
        //        var fileName = Path.GetFileName(file.FileName);
        //        var path = Path.Combine(Server.MapPath("~/content/pics"), fileName);
        //        file.SaveAs(path);
        //    }
        //    return RedirectToAction("Upload");
        //}
        
    }
}