﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP.MVC.Test.Web.Controllers
{
    public class ApplicationErrorController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        // GET: Error
        public ActionResult Error(string error)
        {
            
            if (!string.IsNullOrEmpty(error))
            {
                ViewBag.DetailError = error;
            }
            else
            {
                ViewBag.DetailError = string.Empty;
            }

            return View("Index");
        }

        
    }
}