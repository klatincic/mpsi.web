﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASP.MVC.Test.Web.Models;
using System.Threading.Tasks;
using System.Xml.Schema;
using ASP.MVC.Test.Web.Services;
using ASP.MVC.Test.Web.Services.Requests;

namespace ASP.MVC.Test.Web.Controllers
{
    public class UserController : Controller
    {

        public async Task<ActionResult> Index()
        {

            GetUserRequest request = new GetUserRequest();
            UserService userService = new UserService();
            var returnValue = await Task.Run(async () => await userService.UserInfo(request));
          
            return View(returnValue);
        }
     

            // GET: User
        //[HttpGet]
        //public async Task<ActionResult> UserInfo(UserModel userModel)
        //{

        //    GetUserRequest request = new GetUserRequest();
        //    UserService userService = new UserService();
        //    var returnValue = await Task.Run(async () => await userService.UserInfo(request));

        //    return View(returnValue);


        //}
        
    }
}