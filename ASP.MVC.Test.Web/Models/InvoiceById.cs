﻿using ASP.MVC.Test.Web.Models.ResponseModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ASP.MVC.Test.Web.Models
{
    public class InvoiceById
    {
        [JsonProperty("success")]
        public string Success { get; set; }
        [JsonProperty("httpStatus")]
        public string HttpStatus { get; set; }
        [JsonProperty("data")]
        public Podaci Podaci { get; set; }

       

    }
}