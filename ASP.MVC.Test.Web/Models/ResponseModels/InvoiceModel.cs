﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ASP.MVC.Test.Web.Models.ResponseModels
{
    public class InvoiceModel : BaseModel
    {
        [JsonProperty("data")]
        public List<Podaci> Podaci { get; set; }

        [JsonIgnore]
        public NewInvoiceModel NewInvoiceModel { get; set; } = new NewInvoiceModel();
        [JsonIgnore]
        public CostCenters CostCenters { get; set; }

    }
}