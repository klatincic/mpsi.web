﻿using Newtonsoft.Json;

namespace ASP.MVC.Test.Web.Models.ResponseModels
{
    public class UserModel : BaseModel
    {
        [JsonProperty(PropertyName = "userName")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "firstName")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "companyId")]
        public int CompanyId { get; set; }

        [JsonProperty(PropertyName = "companyCode")]
        public string CompanyCode { get; set; }

        [JsonProperty(PropertyName = "companyName")]
        public string CompanyName { get; set; }
    }
}