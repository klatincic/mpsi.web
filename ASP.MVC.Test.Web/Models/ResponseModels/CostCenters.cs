﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.MVC.Test.Web.Models.ResponseModels
{
    public class CostCenters
    {
        public bool success { get; set; }
        public int httpStatus { get; set; }
        public List<CostCenter> data { get; set; }
    }
}