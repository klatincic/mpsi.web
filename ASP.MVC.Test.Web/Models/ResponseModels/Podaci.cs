﻿using System;
using Newtonsoft.Json;

namespace ASP.MVC.Test.Web.Models.ResponseModels
{
    public class Podaci:BaseModel
    {
        [JsonProperty("invoiceId")]
        public int InvoiceId { get; set; }

        [JsonProperty("paymentType")]
        public string PaymentType { get; set; }

        [JsonProperty("invoiceType")]
        public string InvoiceType { get; set; }

        [JsonProperty("company")]
        public Company CompanyData { get; set; }

        [JsonProperty("createdBy")]
        public CreatedBy CreatedByData { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("costCenter")]
        public CostCenter CostCenterData { get; set; }

        [JsonProperty("totalAmount")]
        public double TotalAmount { get; set; }

        [JsonProperty("invoiceNumber")]
        public string InvoiceNumber { get; set; }

        [JsonProperty("invoiceDate")]
        public DateTime InvoiceDate { get; set; }
    }
}