﻿using Newtonsoft.Json;

namespace ASP.MVC.Test.Web.Models.ResponseModels
{
    public class Company
    {
        [JsonProperty("companyId")]
        public int CompanyId { get; set; }

        [JsonProperty("companyCode")]
        public string CompanyCode { get; set; }

        [JsonProperty("companyName")]
        public string CompanyName { get; set; }
    }
}