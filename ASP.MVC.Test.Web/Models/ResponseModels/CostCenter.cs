﻿using Newtonsoft.Json;

namespace ASP.MVC.Test.Web.Models.ResponseModels
{
    public class CostCenter
    {
        [JsonProperty("costCenterId")]
        public int CostCenterId { get; set; }

        [JsonProperty("costCenterCode")]
        public string CostCenterCode { get; set; }

        [JsonProperty("costCenterName")]
        public string CostCenterName { get; set; }
    }
}