﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ASP.MVC.Test.Web.Models
{
    public class NewInvoiceModel
    {
        [Required]
        [Display(ResourceType = typeof(Resources.Language), Name = "CommonInvoiceType")]
        public string InvoiceType { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Language), Name = "CommonPaymentType")]
        public string PaymentType { get; set; }

        [Display(ResourceType = typeof(Resources.Language), Name = "Comment")]
        public string Comment { get; set; }

        [Display(ResourceType = typeof(Resources.Language), Name = "CommonPaymentType")]
        public int CoastCenterCode { get; set; }

        // [Display(ResourceType = typeof(Resources.Language), Name = "ImgUpload")]
        //public string InvoiceImage { get; set; }
        //[Display(ResourceType = typeof(Resources.Language), Name = "ImgUpload")]
        //[Required(ErrorMessage = "Please select file.")]
        // [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif)$", ErrorMessage = "Only Image files allowed.")]
        // public HttpPostedFileBase InvoiceImage { get; set; }

    }
}