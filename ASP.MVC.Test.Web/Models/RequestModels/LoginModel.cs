﻿using ASP.MVC.Test.Web.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP.MVC.Test.Web.Models
{
    public class LoginModel : BaseModel
    {
        [Display(ResourceType = typeof(Resources.Language), Name = "CommonUserName")]
        public string UserName
        {
            get; set;
        }

        [Display(ResourceType = typeof(Resources.Language), Name = "CommonPassword")]
        public string Password { get; set; }

    }
}