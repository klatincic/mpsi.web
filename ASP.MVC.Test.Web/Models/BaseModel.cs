﻿
using System.Collections.Generic;
using Newtonsoft.Json;

namespace ASP.MVC.Test.Web.Models
{
    public class BaseModel
    {
        [JsonProperty(PropertyName= "success")]
        public bool Success { get; set; }

        [JsonProperty(PropertyName = "httpStatus")]
        public int HttpStatus { get; set; }

        [JsonProperty(PropertyName = "pageNumber")]
        public int PageNumber { get; set; }


        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }

        [JsonProperty(PropertyName = "error_description")]
        public string ErrorDescription { get; set; }
    }
}